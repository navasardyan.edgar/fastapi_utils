from typing import Any, Iterable
from typing import Generic
from typing import TypedDict
from typing import TypeVar
from uuid import UUID

from sqlalchemy import delete
from sqlalchemy import false
from sqlalchemy import func
from sqlalchemy import insert
from sqlalchemy import null
from sqlalchemy import select
from sqlalchemy import UnaryExpression
from sqlalchemy import update
from sqlalchemy.sql.selectable import Select
from sqlalchemy.sql.selectable import TypedReturnsRows
from fastapi_utils.pagination import OrderBy
from fastapi_utils.models import BaseModel
from fastapi_utils.models import SoftDeletableModelMixin
from fastapi_utils.models import UpdatableModelMixin

ModelType = TypeVar('ModelType', bound=BaseModel)
QueryT = TypeVar('QueryT', bound=Select[tuple[Any]])


class Filter(TypedDict):
    field: str
    op: str
    value: Any | None


class BaseRepo(Generic[ModelType]):
    def __init__(self, model: type[BaseModel], default_limit: int = 20):
        self.model = model
        self.default_limit = default_limit

    def get_order_by_mapping(self) -> dict[str, UnaryExpression[Any]]:
        rs: dict[str, UnaryExpression[Any]] = {
            '+created_at': self.model.created_at.asc(),
            '-created_at': self.model.created_at.desc(),
        }
        if issubclass(self.model, UpdatableModelMixin):
            rs.update({
                '+updated_at': self.model.updated_at.asc(),
                '-updated_at': self.model.updated_at.desc(),
            })
        return rs

    def create(self) -> TypedReturnsRows[tuple[ModelType]]:
        return insert(self.model).returning(self.model)

    def _get_query_modifiers(
            self,
            filters: Iterable[Filter] = (),
            order_by: OrderBy | None = None,
    ) -> tuple[list[Any], list[Any]]:
        predicates = []
        sorts = []
        filters = list(filters)
        if issubclass(self.model, SoftDeletableModelMixin):
            filters.append(
                {'field': 'deleted_at', 'op': 'is', 'value': null()})
        for filter_ in filters:
            field_name = filter_['field']
            field = getattr(self.model, field_name)
            dict_filter = dict(filter_)
            dict_filter.pop('field')
            match filter_:
                case {'op': _, 'value': None}:
                    continue
                case {'op': 'eq' | '=', 'value': value}:
                    predicates.append(field == value)
                case {'op': 'is', 'value': value}:
                    predicates.append(field.is_(value))
                case {'op': 'ilike' | '~=', 'value': value}:
                    predicates.append(field.ilike(f'%{value}%'))
                case {'field': _, 'op': 'in', 'value': [] | () | set(())}:
                    predicates.append(false())
                case {'op': 'in', 'value': value}:
                    predicates.append(field.in_(value))
                case {'op': 'contains', 'value': value}:
                    predicates.append(field.contains(value))
                case {'op': 'gt' | '>', 'value': value}:
                    predicates.append(field > value)
                case {'op': 'ge' | '>=', 'value': value}:
                    predicates.append(field >= value)
                case {'op': 'lt' | '<', 'value': value}:
                    predicates.append(field < value)
                case {'op': 'le' | '<=', 'value': value}:
                    predicates.append(field <= value)
                case _ as otherwise:
                    assert False, str(otherwise)
        if order_by is not None:
            mapping = self.get_order_by_mapping()
            sorts.append(mapping[order_by])
        return predicates, sorts

    def list(
            self,
            filters: Iterable[Filter] = (),
            order_by: Any | None = None,
            limit: int | None = None,
            offset: int = 0,
            extra_select: Iterable[Any] = (),
    ) -> Select[tuple[BaseModel]]:
        query = select(self.model, *extra_select)
        predicates, sorts = self._get_query_modifiers(filters, order_by)
        for predicate in predicates:
            query = query.where(predicate)
        for sort in sorts:
            query = query.order_by(sort)
        return query.limit(limit or self.default_limit).offset(offset)

    def count(
            self,
            filters: Iterable[Filter] = (),
    ) -> Select[tuple[int]]:
        query = select(func.count()).select_from(self.model)
        predicates, _ = self._get_query_modifiers(filters)
        for predicate in predicates:
            query = query.where(predicate)
        return query

    def retrieve(self, **kwargs) -> Select[tuple[BaseModel]]:
        if not kwargs:
            raise ValueError('At least one argument should be passed')
        query = select(self.model)
        for field_name, value in kwargs.items():
            try:
                field = getattr(self.model, field_name)
            except AttributeError:
                raise ValueError(f"Field '{field_name}' does not exist in the model '{self.model.__name__}'")
            else:
                query = query.where(field == value)
        if issubclass(self.model, SoftDeletableModelMixin):
            query = query.where(self.model.deleted_at.is_(None))
        return query

    def retrieve_all(self) -> Select[tuple[BaseModel]]:
        query = select(self.model)
        if issubclass(self.model, SoftDeletableModelMixin):
            query = query.where(self.model.deleted_at.is_(null()))
        return query

    def update(
            self,
            entity_id: UUID,
            data: dict[str, object],
    ) -> TypedReturnsRows[tuple[BaseModel]]:
        query = (
            update(self.model)
            .where(self.model.id == entity_id)
            .values(**data)
            .returning(self.model)
        )
        if issubclass(self.model, SoftDeletableModelMixin):
            query = query.where(self.model.deleted_at.is_(null()))
        return query

    def delete(self, entity_id: UUID) -> TypedReturnsRows[tuple[BaseModel]]:
        return self.delete_by(
            filters=[Filter(field='id', op='=', value=entity_id)])

    def delete_by(
            self,
            filters: Iterable[Filter] =
            ()) -> TypedReturnsRows[tuple[BaseModel]]:
        query = delete(self.model)
        predicates, _ = self._get_query_modifiers(filters)
        for predicate in predicates:
            query = query.where(predicate)
        return query.returning(self.model)
