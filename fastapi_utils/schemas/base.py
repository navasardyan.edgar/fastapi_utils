from typing import Any
from uuid import UUID
from pydantic import root_validator
from py_utils.schemas.base import BaseSchema


class BaseResponseSchema(BaseSchema):
    id: UUID

    class Config:
        orm_mode = True


class SuccessResponseSchema(BaseSchema):
    status: str = 'OK'


class BaseFilterSchema(BaseSchema):
    id: UUID | None

    @root_validator(pre=True)
    def check_any_field_presented(
            cls, values: dict[str, Any]) -> (
            dict)[str, Any]:  # noqa: N805
        attrs = cls.schema()['properties'].keys()
        clean_values = {k: v for k, v in values.items() if (
            k in attrs and v is not None)}
        if not clean_values:
            raise ValueError(
                'Empty filter. At least one update field must be set.')
        return clean_values
