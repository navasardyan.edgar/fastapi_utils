from collections.abc import Callable
from collections.abc import Iterator
import re

from pydantic.errors import PydanticValueError

string_without_zero_byte_delimiter = re.compile(r'^[^\x00]*$')


class ZeroBytePrefixError(PydanticValueError):
    msg_template = 'string must not contain zero byte prefix'


def safe_string_validator(value: str) -> str:
    if not string_without_zero_byte_delimiter.match(value):
        raise ZeroBytePrefixError()

    return value


class PGSafeStr(str):
    @classmethod
    def __get_validators__(cls) -> Iterator[Callable[[str], str]]:
        yield safe_string_validator
