import uuid
import sqlalchemy as sa
from sqlalchemy import text
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import DeclarativeBase


class Base(DeclarativeBase):
    pass


class CreatableModelMixin:
    __abstract__ = True

    created_at = sa.Column(
        sa.DateTime,
        server_default=sa.func.now(),
        nullable=False,
    )


class UpdatableModelMixin:
    __abstract__ = True

    updated_at = sa.Column(
        sa.DateTime,
        server_default=sa.func.now(),
        onupdate=sa.func.now(),
    )


class SoftDeletableModelMixin:
    __abstract__ = True

    deleted_at = sa.Column(sa.DateTime)
    is_active = sa.Column(sa.Boolean, server_default='true')


class BaseModel(Base):
    __abstract__ = True

    id = sa.Column(
        UUID,
        primary_key=True,
        server_default=text('uuid_generate_v4()'),
    )

    def id_as_uuid(self) -> uuid.UUID:
        return uuid.UUID(str(self.id))


class BaseSoftDeletableModel(BaseModel, SoftDeletableModelMixin):
    __abstract__ = True
