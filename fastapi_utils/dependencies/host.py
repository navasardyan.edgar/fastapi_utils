from typing import cast

from fastapi import Request
from starlette.datastructures import Address


def host_dependency(request: Request) -> str | None:
    if 'X-Forwarded-For' in request.headers:
        x_forwarded_for = request.headers['X-Forwarded-For']
        return x_forwarded_for.split(',')[0]
    elif 'Remote-Addr' in request.headers:
        return request.headers['Remote-Addr']
    else:
        return cast(Address, request.client).host
