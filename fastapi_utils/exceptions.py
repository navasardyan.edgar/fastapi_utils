from typing import Any


class FKNotFoundError(Exception):
    def __init__(self, message: str, fk_name: str, table_name: Any):
        super().__init__(message)
        self.fk_name = fk_name
        self.table_name = table_name


class ConflictError(Exception):
    def __init__(self, message: str):
        super().__init__(message)


class AlreadyExistsError(Exception):
    def __init__(self, message: str, constraint_name: str):
        super().__init__(message)
        self.constraint_name = constraint_name


class NotFoundError(Exception):
    def __init__(self, message: str, entity_id: Any):
        super().__init__(message)
        self.entity_id = entity_id


class InvalidLookupParamsError(Exception):
    def __init__(self, table_name: Any, **kwargs: Any):
        message = (f'Could not find a record in table'
                   f' {table_name} corresponding to ')
        message += ','.join([f'{k}={v}' for k, v in kwargs.items()])
        super().__init__(message)


class NoDataPassedError(Exception):
    def __init__(self, message: str):
        super().__init__(message)


class EntityNotFoundError(Exception):
    def __init__(self, message: str):
        super().__init__(message)


class MultipleEntitiesFoundError(Exception):
    def __init__(self, message: str):
        super().__init__(message)
