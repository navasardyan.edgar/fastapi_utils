from typing import TypeAlias, Literal

from fastapi import Query
from py_utils.schemas.base import BaseSchema


OrderBy: TypeAlias = Literal['+created_at',
                             '+updated_at', '-created_at', '-updated_at']


class BasePagination(BaseSchema):
    limit: int = Query(20, ge=1, le=50)
    offset: int = Query(0, ge=0, le=1000)


class Pagination(BasePagination):
    order_by: OrderBy = '-updated_at'
