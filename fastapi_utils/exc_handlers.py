from http import HTTPStatus

from fastapi import Request
from fastapi.responses import JSONResponse

from fastapi_utils.exceptions import AlreadyExistsError
from fastapi_utils.exceptions import ConflictError
from fastapi_utils.exceptions import FKNotFoundError
from fastapi_utils.exceptions import InvalidLookupParamsError
from fastapi_utils.exceptions import NoDataPassedError
from fastapi_utils.exceptions import NotFoundError
from fastapi_utils.exceptions import EntityNotFoundError
from fastapi_utils.exceptions import MultipleEntitiesFoundError


async def handle_fk_not_found_error(
        request: Request, exc: FKNotFoundError) -> JSONResponse:
    return JSONResponse(
        status_code=HTTPStatus.BAD_REQUEST,
        content={'detail': exc.args[0]},
    )


async def handle_conflict_error(
        request: Request,
        exc: ConflictError) -> JSONResponse:
    return JSONResponse(
        status_code=HTTPStatus.CONFLICT,
        content={'detail': exc.args[0]},
    )


async def handle_already_exists_error(
        request: Request, exc: AlreadyExistsError) -> JSONResponse:
    return JSONResponse(
        status_code=HTTPStatus.CONFLICT,
        content={'detail': exc.args[0]},
    )


async def handle_not_found_error(
        request: Request, exc: NotFoundError) -> JSONResponse:
    return JSONResponse(
        status_code=HTTPStatus.NOT_FOUND,
        content={'detail': exc.args[0]},
    )


async def handle_invalid_lookup_params_error(
        request: Request, exc: InvalidLookupParamsError,
) -> JSONResponse:
    return JSONResponse(
        status_code=HTTPStatus.BAD_REQUEST,
        content={'detail': exc.args[0]},
    )


async def handler_no_data_passed_error(
        request: Request,
        exc: NoDataPassedError,
) -> JSONResponse:
    return JSONResponse(
        status_code=HTTPStatus.BAD_REQUEST,
        content={'detail': exc.args[0]},
    )


async def handle_entity_not_found_error(
        request: Request,
        exc: EntityNotFoundError
) -> JSONResponse:
    return JSONResponse(
        status_code=HTTPStatus.NOT_FOUND,
        content={'detail': exc.args[0]},
    )


async def handle_multiple_entities_found_error(
        request: Request,
        exc: MultipleEntitiesFoundError
) -> JSONResponse:
    return JSONResponse(
        status_code=HTTPStatus.MULTIPLE_CHOICES,
        content={'detail': exc.args[0]},
    )
