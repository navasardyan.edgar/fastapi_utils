import re
from collections.abc import Iterable
from http import HTTPStatus
from typing import Any
from typing import cast
from typing import Generic
from typing import List
from typing import TypeVar
from uuid import UUID

from fastapi import HTTPException
from sqlalchemy import exc
from sqlalchemy import func
from sqlalchemy.exc import NoResultFound, MultipleResultsFound
from sqlalchemy.ext.asyncio import AsyncSession

from fastapi_utils.models import BaseModel
from fastapi_utils.models import SoftDeletableModelMixin
from fastapi_utils.repos.base import BaseRepo
from fastapi_utils.repos.base import Filter
from fastapi_utils.exceptions import AlreadyExistsError, EntityNotFoundError, MultipleEntitiesFoundError
from fastapi_utils.exceptions import FKNotFoundError
from fastapi_utils.exceptions import NotFoundError
from fastapi_utils.resources import strings
from py_utils.schemas import BaseSchema
from fastapi_utils.schemas import BaseResponseSchema


ModelClass = TypeVar('ModelClass', bound=BaseModel)
ModelResponseSchema = TypeVar('ModelResponseSchema', bound=BaseResponseSchema)


class BaseCRUDService(Generic[ModelClass, ModelResponseSchema]):
    response_schema: type[ModelResponseSchema] = NotImplemented

    _unique_violation_errors: dict[str, str] = {}
    _fk_violation_errors: dict[str, str] = {}
    _not_found_error: str = 'Entity not found'

    _unique_constraint_pattern = re.compile(
        r'.*duplicate key value violates unique constraint "(?P<name>\w+)"',
    )
    _fk_constraint_pattern = re.compile(
        r'.*insert or update on table "(?P<table_name>\w+)" '
        r'violates foreign key constraint "(?P<fk_name>\w+)"',
    )

    def __init__(self, repo: BaseRepo[ModelClass]):
        self.repo = repo

    def serialize(self, db_result: BaseModel) -> ModelResponseSchema:
        return self.response_schema.from_orm(db_result)

    def serialize_many(
            self,
            db_result: list[BaseModel]) -> list[ModelResponseSchema]:
        return [self.serialize(item) for item in db_result]

    async def create(
            self,
            session: AsyncSession,
            data: BaseSchema,
    ) -> ModelResponseSchema:
        query = self.repo.create()
        try:
            created = await session.execute(query, data.dict())
        except exc.IntegrityError as ex:
            raise self._reraise_integrity_error(ex) from ex
        result = created.scalar_one()
        return self.serialize(result)

    async def retrieve(self, db_session: AsyncSession, **kwargs):
        query = self.repo.retrieve(**kwargs)
        result = await db_session.execute(query)
        try:
            entity = result.scalars().one()
        except NoResultFound:
            raise EntityNotFoundError(f"{self._not_found_error}. Filters used: {kwargs}")
        except MultipleResultsFound:
            raise MultipleEntitiesFoundError("Multiple entities found matching the criteria")
        return self.serialize(entity)

    async def retrieve_all(
            self,
            session: AsyncSession) -> List[ModelResponseSchema]:
        query = self.repo.retrieve_all()
        entities = await session.execute(query)
        return [
            self.response_schema.from_orm(result) for
            result in entities.scalars().all()]

    async def list(
            self,
            session: AsyncSession,
            filters: Iterable[Filter] = (),
            order_by: Any | None = None,
            limit: int | None = None,
            offset: int = 0,
    ) -> list[ModelResponseSchema]:
        query = self.repo.list(filters, limit=limit,
                               order_by=order_by, offset=offset)
        entities = await session.execute(query)
        results = cast(list[BaseModel], entities.scalars())
        return self.serialize_many(results)

    async def count(
            self,
            session: AsyncSession,
            filters: Iterable[Filter] = (),
    ) -> int:
        query = self.repo.count(filters)
        result = await session.execute(query)
        return result.scalar_one()

    async def patch(
            self,
            session: AsyncSession,
            entity_id: UUID,
            data: BaseSchema,
    ) -> ModelResponseSchema:
        data_dict = data.dict(exclude_unset=True)
        if not data_dict:
            raise HTTPException(
                status_code=HTTPStatus.BAD_REQUEST,
                detail=strings.INSUFFICIENT_CLIENT_DATA,
            )
        query = self.repo.update(entity_id, data_dict)
        entity = await session.execute(query)
        result = entity.scalar_one_or_none()
        if result is None:
            raise NotFoundError(self._not_found_error, entity_id=entity_id)
        return self.serialize(result)

    async def delete(
            self, session:
            AsyncSession,
            entity_id: UUID) -> ModelResponseSchema:
        if issubclass(self.repo.model, SoftDeletableModelMixin):
            query = self.repo.update(entity_id, {'deleted_at': func.now()})
        else:
            query = self.repo.delete(entity_id)
        entity = await session.execute(query)
        result = entity.scalar_one_or_none()
        if result is None:
            raise NotFoundError(self._not_found_error, entity_id=entity_id)
        return self.serialize(result)

    def _reraise_integrity_error(self, ex: exc.IntegrityError) -> Exception:
        if result := self._fk_constraint_pattern.match(ex.args[0]):
            fk_name = result.group('fk_name')
            table_name = result.group('table_name')
            return FKNotFoundError(
                self._fk_violation_errors.get(
                    fk_name, 'Related object not found'),
                fk_name=fk_name,
                table_name=table_name,
            )
        if result := self._unique_constraint_pattern.match(ex.args[0]):
            constraint = result.group('name')
            return AlreadyExistsError(
                self._unique_violation_errors.get(
                    constraint, 'Entity already exists'),
                constraint_name=constraint,
            )
        return ex
