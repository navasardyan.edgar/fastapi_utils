import logging

from sqlalchemy.ext.asyncio import async_sessionmaker
from sqlalchemy.ext.asyncio import AsyncSession
from starlette.types import ASGIApp
from starlette.types import Receive
from starlette.types import Scope
from starlette.types import Send

log = logging.getLogger(__name__)


class DBSessionMiddleware:
    def __init__(
            self,
            app: ASGIApp,
            session_factory: async_sessionmaker[AsyncSession]):
        self.session_factory = session_factory
        self.app = app

    async def __call__(
            self,
            scope: Scope,
            receive: Receive,
            send: Send) -> None:
        if scope['type'] == 'http':
            scope['db_session'] = self.session_factory()
            try:
                await self.app(scope, receive, send)
            finally:
                db_session = scope.pop('db_session', None)
                if db_session is not None:
                    await db_session.close()
            return
        await self.app(scope, receive, send)
